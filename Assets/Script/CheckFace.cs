using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckFace : MonoBehaviour
{
    private bool checkFaces = false;
    public GameObject[] allObjs;
    public List<GameObject> faceObjs = new List<GameObject>();
    private GameObject currentFace;
    [HideInInspector]
    public RectTransform faceRect;
    public bool faceShowing = false;

    void Start()
    {
        StartCoroutine(InitFaces());
    }

    private void FindAll()
    {
        Object[] tempList = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        List<GameObject> realList = new List<GameObject>();
        GameObject temp;

        foreach (Object obj in tempList)
        {
            if (obj is GameObject)
            {
                temp = (GameObject)obj;
                if (temp.hideFlags == HideFlags.None)
                    realList.Add((GameObject)obj);
            }
        }
        allObjs = realList.ToArray();
    }

    public void FacesCheck()
    {
        for (int i = 0; i < faceObjs.Count; i++)
        {
            if (faceObjs[i].activeSelf)
            {
                currentFace = faceObjs[i];
                faceRect = currentFace.GetComponent<RectTransform>();
                faceShowing = true;
                //set captioning vis to true
            }
            else
            {
                faceShowing = false;
                //set captioning vis to false
            }
        }
    }
    private IEnumerator InitFaces()
    {
        yield return new WaitForSeconds(1);
        FindAll();
        foreach(GameObject obj in allObjs)
        {
            if(obj.name == "KeyPointsMarker(Clone)")
            {
                faceObjs.Add(obj);
            }
        }
        yield return new WaitForSeconds(1);
        if (faceObjs != null)
        {
            checkFaces = true;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (checkFaces)
        {
            FacesCheck();
        }
    }    
}
