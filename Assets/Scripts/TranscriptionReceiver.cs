using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using Oculus.Voice.Dictation;
using UnityEngine.Windows.Speech;

public class TranscriptionReceiver : MonoBehaviour
{
    public Text textDisp;
    public RectTransform backing;
    //public AppDictationExperience dictScript;
    private RectTransform myRect;
    public CheckFace checkFaceScript;
    private string currentString;
    public string[] animNames;
    string[] words;
    private DictationRecognizer dictationRecognizer;//windows is better than the Meta voice SDK
    public List<string> animQueue = new List<string>();
    public Animation handsAnim;
    public float smoothTime = 0.1F;
    private Vector2 velocity = Vector2.zero;
    public Transform hands;
    private Vector3 originHandScale;

    private void Start()
    {
        /*dictScript.TranscriptionEvents.OnPartialTranscription.AddListener(ProcessTranscription);
        dictScript.Activate();*/
        originHandScale = hands.localScale;
        StartCoroutine(StartRecordingWhenPossible());
        myRect = gameObject.GetComponent<RectTransform>();
    }
    public void ProcessTranscription(string transcript)
    {
        textDisp.text = transcript;
        currentString = transcript.ToLower();
        backing.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, textDisp.gameObject.GetComponent<RectTransform>().rect.width/10 + 25);
        //split spoken sentence into words
         words = currentString.Split(' ');
        foreach(string word in words)
        {
            foreach (string animWord in animNames)
            {
                if(word == animWord)
                {
                    animQueue.Add(word);
                }
            }
        }
    }

    private IEnumerator WaitToDeque()
    {
        yield return new WaitForSeconds(1);
        animQueue.RemoveAt(animQueue.Count - 1);
    }

    private void FixedUpdate()
    {
        if (checkFaceScript.faceShowing)
        {
            //it's the scaling!!!!
            myRect.position = checkFaceScript.faceRect.position;
            //myRect.position = Vector2.SmoothDamp(myRect.position, checkFaceScript.faceRect.position, ref velocity, smoothTime);
            
            hands.transform.localScale = originHandScale;
            hands.transform.rotation = Quaternion.Euler(0, hands.transform.rotation.y, hands.transform.rotation.z);
        }
        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            dictScript.TranscriptionEvents.OnPartialTranscription.AddListener(ProcessTranscription);
            dictScript.Activate();
            Debug.Log("Reset");

        }*/
        if (animQueue.Count != 0)
        {
            if (!handsAnim.isPlaying)
            {
                handsAnim.Play(animQueue[animQueue.Count - 1]);
                StartCoroutine(WaitToDeque());
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnRefresh();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    /*void LateUpdate()
    {
        Vector3 tmpPos = myRect.position;
        tmpPos.z = Mathf.Clamp(myRect.position.x, myRect.position.y, 10);
        myRect.transform.position = tmpPos;
    }*/

    void onDictationResult(string text, ConfidenceLevel confidence)
    {
        //Debug.LogFormat("Dictation result: " + text);
        //p1Text.text = string.Format("{0}", text);
    }

    void onDictationHypothesis(string text)
    {
        // write your logic here
        //Debug.LogFormat("Dictation hypothesis: {0}", text);
        ProcessTranscription(text);
    }

    void onDictationComplete(DictationCompletionCause cause)
    {
        // write your logic here
        /*if (cause != DictationCompletionCause.Complete)
            Debug.LogErrorFormat("Dictation completed unsuccessfully: {0}.", cause);*/
    }

    void onDictationError(string error, int hresult)
    {
        // write your logic here
        //Debug.LogErrorFormat("Dictation error: {0}; HResult = {1}.", error, hresult);
    }
    private void ResetRecording()
    {
        if (dictationRecognizer.Status == SpeechSystemStatus.Failed || dictationRecognizer.Status == SpeechSystemStatus.Stopped)
        {
            dictationRecognizer = new DictationRecognizer();

            dictationRecognizer.DictationResult += onDictationResult;
            dictationRecognizer.DictationHypothesis += onDictationHypothesis;
            dictationRecognizer.DictationComplete += onDictationComplete;
            dictationRecognizer.DictationError += onDictationError;

            dictationRecognizer.Start();
        }
        else
        {
            // Shutdown the PhraseRecognitionSystem. This controls the KeywordRecognizers.
            PhraseRecognitionSystem.Shutdown();
            dictationRecognizer.Stop();
            dictationRecognizer.Dispose();
            UnityMainThreadDispatcher.Instance().Enqueue(StartRecordingWhenPossible());
        }
    }
    public void OnRefresh()
    {
        ResetRecording();
    }

    private IEnumerator StartRecordingWhenPossible()
    {
        while (PhraseRecognitionSystem.Status == SpeechSystemStatus.Running)
        {
            yield return null;
        }
        dictationRecognizer = new DictationRecognizer();

        dictationRecognizer.DictationResult += onDictationResult;
        dictationRecognizer.DictationHypothesis += onDictationHypothesis;
        dictationRecognizer.DictationComplete += onDictationComplete;
        dictationRecognizer.DictationError += onDictationError;

        dictationRecognizer.Start();
    }
}
